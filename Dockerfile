FROM openjdk:8
EXPOSE 9001
ADD target/clientui-0.0.1-SNAPSHOT.jar clientui.jar
ENTRYPOINT ["java","-jar","/clientui-0.0.1-SNAPSHOT.jar"]
